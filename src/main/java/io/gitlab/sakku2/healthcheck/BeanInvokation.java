package io.gitlab.sakku2.healthcheck;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

public class BeanInvokation {


    public static void checkBeanInvoke(String key, Object adminClass, boolean isRuntime, String methodName, List<Object> params){

        try {
            List<Class> types = params.stream().map(o -> o.getClass()).collect(Collectors.toList());

            Class[] typeArray = new Class[types.size()];

            typeArray = types.toArray(typeArray);
            fixParams(typeArray);

            if(isRuntime){
                if(key.equals(Consts.CMD)) {
                    Runtime runtime = Runtime.getRuntime();
                    invoke(runtime, methodName, params);
                }
            }else {
                invoke(adminClass, methodName, params);
            }

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

    }

    private static void fixParams(Class[] typeArray) {
        for (int i=0; i< typeArray.length; i++){
          if(typeArray[i].getName().equals(Consts.CHECK_INT)){
              typeArray[i] = Integer.TYPE;
          }else if(typeArray[i].getName().equals(Consts.CHECK_LONG)){
              typeArray[i] = Long.TYPE;
          }else if (typeArray[i].getName().equals(Consts.CHECK_DOUBLE)){
              typeArray[i] = Double.TYPE;
          }else if (typeArray[i].getName().equals(Consts.CHECK_FLOAT)){
              typeArray[i] = Float.TYPE;
          }
        }
    }

    private static void invoke(Object adminClass, String methodName, List<Object> params) {
        Method[] methods = adminClass.getClass().getMethods();
        for (Method m : methods) {
            if (m.getName().equals(methodName)) {
                try {
                    m.invoke(adminClass, params.toArray());
                } catch (Exception ignored) {
                }
            }
        }
    }


    private static boolean runGC(boolean force){
        Runtime rt = Runtime.getRuntime();
        if(rt.freeMemory()/rt.totalMemory() < Consts.MEMORY_GC_THRESHOLD || force){
            rt.gc();
            return true;
        }
        return false;
    }

    private static int getProcessorCount(){
        Runtime rt = Runtime.getRuntime();
        return rt.availableProcessors();
    }


}
