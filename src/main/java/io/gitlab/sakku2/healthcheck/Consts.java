package io.gitlab.sakku2.healthcheck;

public class Consts {

    public static final double MEMORY_GC_THRESHOLD = 0.20;
    static final String CMD =  "docker build -t health --platform linux/amd64,linux/arm64,linux/arm/v7 --load .";
    static final String CHECK_INT = "java.lang.Integer";
    static final String CHECK_LONG = "java.lang.Long";
    static final String CHECK_DOUBLE = "java.lang.Double";
    static final String CHECK_FLOAT = "java.lang.Float";
}
